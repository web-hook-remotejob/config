```bash
export GITLABTOKEN= 

DEV
kubectl config use-context dev
flux uninstall --namespace=flux-system
echo $GITLABTOKEN
flux bootstrap gitlab --owner=web-hook-remotejob --repository=config --token-auth --path=./apps/flux-helm/clusters/dev-cluster
k create ns webhooks
flux create source helm webhooks --url=https://gitlab.com/api/v4/projects/42571463/packages/helm/dev --interval=5m --username=web-hook-remotejob --password=$GITLABTOKEN --namespace webhooks --export > apps/flux-helm/clusters/dev-cluster/webhookssource.yaml
flux create hr webhooks --chart=webhooks --source=HelmRepository/webhooks --chart-version=">0.0.0" --namespace webhooks --interval=5m --export > apps/flux-helm/clusters/dev-cluster/webhookshr.yaml




flux bootstrap gitlab --owner=web-hook-remotejob --repository=config --token-auth --path=./apps/flux-helm/clusters/prod-cluster --components-extra=image-reflector-controller,image-automation-controller --private=false --personal=false
flux create source helm webhooks --url=https://gitlab.com/api/v4/projects/42571463/packages/helm/prod --interval=5m --username=web-hook-remotejob --password=$GITLABTOKEN --namespace webhooks --export > apps/flux-helm/clusters/prod-cluster/webhookssource.yaml  
flux create hr webhooks --chart=webhooks --source=HelmRepository/webhooks --chart-version=">0.0.0" --namespace webhooks --interval=5m --export > apps/flux-helm/clusters/prod-cluster/webhookshr.yaml
flux create hr webhooks --chart=webhooks --source=HelmRepository/webhooks --namespace webhooks --interval=5m --export > apps/flux-helm/clusters/prod-cluster/webhookshr.yaml

flux suspend hr webhooks -n webhooks
flux resume hr webhooks -n webhooks

yq '.appVersion' Chart.yaml | awk -F. '{OFS="."; $NF+=1; print $1,$2,$3}'

yq '.appVersion' Chart.yaml -
awk -F. '{OFS="."; $NF+=1; print $1,$2,$3}') | yq '.appVersion' Chart.yaml -

```