HVER=` yq '.version' apps/webhooks-prod/Chart.yaml | awk -F. '{OFS="."; $NF+=1; print $1,$2,$3}'`
echo $HVER
yq -i ".version=\"${HVER}\"" apps/webhooks-prod/Chart.yaml

# yq -i ".${CI_PROJECT_NAME}.tag=\"${CI_COMMIT_TAG}\"" ./apps/webhooks-prod/values.yaml
